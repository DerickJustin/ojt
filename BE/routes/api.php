<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\student_controller;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth', 'middleware'=>'api'], function () {
    Route::post('create', [student_controller::class, 'create']);
    Route::post('me', [student_controller::class, 'me']);
    Route::post('login', [student_controller::class, 'login']);
    Route::post('logout', [student_controller::class, 'logout']);
});




